﻿using Microsoft.Extensions.DependencyInjection;
using XamarinMacModalTest.Interfaces;
using XamarinMacModalTest.macOS.Entites.Services;

namespace XamarinMacModalTest.macOS.Configuration.DI
{
    public static class ServiceCollectionFactory
    {
        public static void AddServices(IServiceCollection services)
        {
            services.AddSingleton<IModalWindowService, ModalWindowService>();
        }
    }
}