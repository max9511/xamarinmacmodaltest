﻿using System.Threading.Tasks;
using AppKit;
using CoreGraphics;
using Xamarin.Forms.Platform.MacOS;
using XamarinMacModalTest.Interfaces;

namespace XamarinMacModalTest.macOS.Entites.Services
{
    public class ModalWindowService : IModalWindowService
    {
        public Task OpenModal()
        {
            CGSize size = new(200, 200);

            NSWindow window = new(new CGRect(new CGPoint(100, 100), size), NSWindowStyle.Closable, NSBackingStore.Buffered, false)
            {
                Title = "Test window",
                TitleVisibility = NSWindowTitleVisibility.Hidden,
                MinSize = size,
                MaxSize = size,
                ContentViewController = new ModalWindow().CreateViewController()
            };

            NSApplication.SharedApplication.RunModalForWindow(window);

            return Task.CompletedTask;
        }
    }
}
