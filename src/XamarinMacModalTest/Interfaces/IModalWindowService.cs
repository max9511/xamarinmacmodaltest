﻿using System.Threading.Tasks;

namespace XamarinMacModalTest.Interfaces
{
    public interface IModalWindowService
    {
        Task OpenModal();
    }
}