﻿using Xamarin.Forms;
using XamarinMacModalTest.ViewModel;

namespace XamarinMacModalTest
{
    public partial class MainPage : ContentPage
    {
        public MainPage(MainPageViewModel mainPageViewModel)
        {
            this.BindingContext = mainPageViewModel;

            InitializeComponent();
        }
    }
}