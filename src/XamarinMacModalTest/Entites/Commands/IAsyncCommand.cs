﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace XamarinMacModalTest.Entites.Commands
{
    /// <summary>
    /// Интерфейс асинхронной команды
    /// </summary>
    /// <typeparam name="T">тип параметра</typeparam>
    public interface IAsyncCommand<in T> : ICommand
    {
        /// <summary>
        /// Выполняет команду асинхронно
        /// </summary>
        /// <param name="parameter">параметр команды</param>
        /// <returns><see cref="Task"/> задача на выполнение метода</returns>
        Task ExecuteAsync(T parameter);

        /// <summary>
        /// Проверяет может ли команды быть выполнена
        /// </summary>
        /// <param name="parameter">параметр команды</param>
        /// <returns>результат проверки</returns>
        bool CanExecute(T parameter);
    }
}