﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using XamarinMacModalTest.Entites.Tasks;

namespace XamarinMacModalTest.Entites.Commands
{
    public class AsyncCommand<T> : IAsyncCommand<T>
    {
        private bool isExecuting;
        private readonly Func<T, Task> execute;
        private readonly Func<T, bool> canExecute;
        private readonly Action<Exception> onException;

        public AsyncCommand(Func<T, Task> execute, Func<T, bool> canExecute = null, Action<Exception> onException = null)
        {
            this.execute = execute;
            this.canExecute = canExecute;
            this.onException = onException;
        }

        public event EventHandler CanExecuteChanged;

        public bool CanExecute(T parameter)
        {
            return this.isExecuting == false && (this.canExecute?.Invoke(parameter) ?? true);
        }

        public async Task ExecuteAsync(T parameter)
        {
            if (this.CanExecute(parameter))
            {
                try
                {
                    this.isExecuting = true;

                    await this.execute(parameter);
                }
                finally
                {
                    this.isExecuting = false;
                }
            }

            this.RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            this.CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Explicit implementations
        bool ICommand.CanExecute(object parameter)
        {
            return this.CanExecute((T)parameter);
        }

        void ICommand.Execute(object parameter)
        {
            this.ExecuteAsync((T)parameter).SafeFireAndForget(onException: this.onException);
        }
        #endregion
    }

}
