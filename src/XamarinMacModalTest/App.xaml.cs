﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Xamarin.Forms;
using XamarinMacModalTest.Configuration.DI;
using XamarinMacModalTest.ViewModel;

namespace XamarinMacModalTest
{
    public partial class App : Application
    {
        public App(Action<IServiceCollection> addPlatformServices)
        {
            InitializeComponent();

            ServiceProvider = ServiceCollectionFactory.SetupServices(addPlatformServices);

            MainPage = new MainPage(ServiceProvider.GetService<MainPageViewModel>());
        }

        public static IServiceProvider ServiceProvider { get; private set; }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}