﻿using System.Threading.Tasks;
using System.Windows.Input;
using XamarinMacModalTest.Entites;
using XamarinMacModalTest.Entites.Commands;
using XamarinMacModalTest.Interfaces;

namespace XamarinMacModalTest.ViewModel
{
    public class MainPageViewModel : BaseViewModel
    {
        private readonly IModalWindowService modalWindowService;

        public MainPageViewModel(IModalWindowService modalWindowService)
        {
            this.modalWindowService = modalWindowService;

            this.OpenModalCommand = new AsyncCommand<object>(this.OnOpenModal);
        }

        public ICommand OpenModalCommand { get; }

        private Task OnOpenModal(object arg)
        {
            return this.modalWindowService.OpenModal();
        }
    }
}