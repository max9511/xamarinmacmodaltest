﻿using System;
using Microsoft.Extensions.DependencyInjection;
using XamarinMacModalTest.ViewModel;

namespace XamarinMacModalTest.Configuration.DI
{
    public static class ServiceCollectionFactory
    {
        public static ServiceProvider SetupServices(Action<IServiceCollection> addPlatformServices)
        {
            ServiceCollection services = new();

            services.AddSingleton<MainPageViewModel>();

            addPlatformServices?.Invoke(services);

            return services.BuildServiceProvider();
        }
    }
}